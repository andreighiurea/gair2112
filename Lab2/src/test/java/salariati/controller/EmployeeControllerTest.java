package salariati.controller;

import com.sun.xml.internal.ws.util.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.Arrays;

import static org.junit.Assert.*;

public class EmployeeControllerTest {
    private EmployeeRepositoryInterface repo = new EmployeeMock();
    private EmployeeValidator valid = new EmployeeValidator();
    private EmployeeController ctrl = new EmployeeController(repo, valid);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addEmployee() throws EmployeeException {
        assertEquals(0, ctrl.getEmployeesList().size());

        ctrl.addEmployee("ABCD", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        assertEquals(1, ctrl.getEmployeesList().size());

        try {
            ctrl.addEmployee("", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        } catch (EmployeeException ex) {
            assertEquals(1, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        try {
            ctrl.addEmployee("Ab", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        } catch (EmployeeException ex) {
            assertEquals(1, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        ctrl.addEmployee("Anc", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        assertEquals(2, ctrl.getEmployeesList().size());

        char[] chars = new char[255];
        Arrays.fill(chars, 'A');
        String name = new String(chars);
        ctrl.addEmployee(name, "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        assertEquals(3, ctrl.getEmployeesList().size());

        chars = new char[254];
        Arrays.fill(chars, 'A');
        name = new String(chars);
        ctrl.addEmployee(name, "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        assertEquals(4, ctrl.getEmployeesList().size());

        chars = new char[256];
        Arrays.fill(chars, 'A');
        name = new String(chars);
        try {
            ctrl.addEmployee(name, "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        } catch (EmployeeException ex) {
            assertEquals(4, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        ctrl.addEmployee("Anb", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "1");
        assertEquals(5, ctrl.getEmployeesList().size());

        try {
            ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "0");
        } catch (EmployeeException ex) {
            assertEquals(5, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        try {
            ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "");
        } catch (EmployeeException ex) {
            assertEquals(5, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        try {
            ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "salariu");
        } catch (EmployeeException ex) {
            assertEquals(5, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "500000");
        assertEquals(6, ctrl.getEmployeesList().size());

        try {
            ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "500001");
        } catch (EmployeeException ex) {
            assertEquals(6, ctrl.getEmployeesList().size());
            assertEquals("Invalid Employee data", ex.getMessage());
        }

        ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "499999");
        assertEquals(7, ctrl.getEmployeesList().size());

        ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "2");
        assertEquals(8, ctrl.getEmployeesList().size());
    }
}