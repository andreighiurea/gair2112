package salariati.main;

import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import sun.management.snmp.jvmmib.EnumJvmMemPoolCollectThreshdSupport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {
    public static DidacticFunction readFunction() {
        System.out.println("    1. ASISTENT");
        System.out.println("    2. LECTURER");
        System.out.println("    3. TEACHER");

        int cmd = 0;
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.print("Choose function: ");
            cmd = in.nextInt();

            switch (cmd) {
                case 1:
                   return DidacticFunction.ASISTENT;
                case 2:
                    return DidacticFunction.LECTURER;
                case 3:
                    return DidacticFunction.TEACHER;
                    default:
                        System.out.println("Wrong option");
            }
        }
    }

    public static void printMenu() {
        System.out.println("1. Add new employee");
        System.out.println("2. Modify employee function");
        System.out.println("3. Show employees ordered");
        System.out.println("4. Exit");
    }

	public static void main(String[] args) {
		
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeValidator validator = new EmployeeValidator();
        EmployeeController employeeController = new EmployeeController(employeesRepository, validator);

		int cmd = 0;
        BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
        Scanner in = new Scanner(System.in);
		while (true) {
            printMenu();
            System.out.print("Choose command: ");
            cmd = in.nextInt();

            switch (cmd) {
                case 1:
                    try {
                        System.out.print("First Name: ");
                        String firstName = buffer.readLine();
                        System.out.print("Last Name: ");
                        String lastName = buffer.readLine();
                        System.out.print("CNP: ");
                        String cnp = buffer.readLine();
                        System.out.print("Salary: ");
                        String salary = buffer.readLine();
                        System.out.println("Function: ");
                        DidacticFunction function = readFunction();

                        employeeController.addEmployee(firstName, lastName, cnp, function, salary);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try {
                        System.out.print("First Name: ");
                        String firstName = buffer.readLine();
                        System.out.print("Last Name: ");
                        String lastName = buffer.readLine();
                        System.out.print("CNP: ");
                        String cnp = buffer.readLine();
                        System.out.print("Salary: ");
                        String salary = buffer.readLine();
                        System.out.println("Old Function: ");
                        DidacticFunction function = readFunction();
                        System.out.println("New Function: ");
                        DidacticFunction functionNew = readFunction();

                        Employee oldEmployee = new Employee(firstName, lastName, cnp, function, salary);
                        Employee newEmployee = new Employee(firstName, lastName, cnp, functionNew, salary);
                        if (!validator.isValid(oldEmployee) || !validator.isValid(newEmployee)) {
                            System.out.println("[ERROR] Data invalid!");
                            break;
                        }
                        employeeController.modifyEmployee(oldEmployee, newEmployee);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    List<Employee> employeeList = employeesRepository.getEmployeeList();

                    for (int i = 0; i < employeeList.size() - 1; i++) {
                        for (int j = i + 1; j < employeeList.size(); j++) {
                            if (Integer.parseInt(employeeList.get(i).getSalary()) < Integer.parseInt(employeeList.get(j).getSalary())) {
                                Employee aux = employeeList.get(i);
                                employeeList.set(i, employeeList.get(j));
                                employeeList.set(j, aux);
                            }
                        }
                    }

                    System.out.println("\n=============Employees==============");
                    for(Employee _employee : employeeList)
                        System.out.println(_employee.toString());
                    System.out.println(  "====================================\n");
                    break;
                case 4:
                    return;
                    default:
                        System.out.println(String.valueOf(cmd) + " Invalid command");

            }
        }
	}

}
