package salariati.controller;

import java.io.IOException;
import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeValidator employeeValidator;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository, EmployeeValidator employeeValidator) {
		this.employeeRepository = employeeRepository;
		this.employeeValidator = employeeValidator;
	}
	
	public void addEmployee(String firstName, String lastName, String cnp, DidacticFunction function, String salary) {
		try {
			Employee employee = new Employee(firstName, lastName, cnp, function, salary);
			if (!employeeValidator.isValid(employee)) {
				System.out.println("[ERROR] Data invalid!");
				return;
			}

			employeeRepository.addEmployee(employee);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}
	
}
