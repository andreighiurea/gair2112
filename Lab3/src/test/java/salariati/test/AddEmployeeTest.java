package salariati.test;

import static org.junit.Assert.*;

import salariati.exception.EmployeeException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMock();
		employeeValidator  = new EmployeeValidator();
		controller         = new EmployeeController(employeeRepository, employeeValidator);
	}
	
	@Test
	public void testRepositoryMock() {
		assertTrue(controller.getEmployeesList().isEmpty());
		assertEquals(0, controller.getEmployeesList().size());
	}
	
	@Test
	public void testAddNewEmployee() throws EmployeeException {
		Employee newEmployee = new Employee("ValidFirstName", "ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee("ValidFirstName", "ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
		assertEquals(1, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

}
