package salariati.repository.implementations;

import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.*;

public class EmployeeImplTest {
    @Test
    public void modifyEmployeeValid() throws EmployeeException {
        EmployeeRepositoryInterface repo = new EmployeeMock();
        EmployeeValidator valid = new EmployeeValidator();
        EmployeeController ctrl = new EmployeeController(repo, valid);

        assertEquals(0, ctrl.getEmployeesList().size());

        ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        assertEquals(1, ctrl.getEmployeesList().size());

        Employee emp = new Employee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        Employee empNew = new Employee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.LECTURER, "5700");

        boolean result;
        result = repo.modifyEmployee(emp, empNew);
        assertTrue(result);
    }

    @Test
    public void modifyEmployeeNonValid() throws EmployeeException {
        EmployeeRepositoryInterface repo = new EmployeeMock();
        EmployeeValidator valid = new EmployeeValidator();
        EmployeeController ctrl = new EmployeeController(repo, valid);

        assertEquals(0, ctrl.getEmployeesList().size());

        ctrl.addEmployee("Andrei", "Ghiurea", "1234567890123", DidacticFunction.TEACHER, "5700");
        assertEquals(1, ctrl.getEmployeesList().size());

        Employee emp2 = new Employee("Un", "Nume", "123", DidacticFunction.TEACHER, "345");
        Employee emp2New = new Employee("Un", "Nume", "123", DidacticFunction.LECTURER, "345");

        boolean result;
        result = repo.modifyEmployee(emp2, emp2New);
        assertFalse(result);
    }

    @Test
    public void modifyEmployeeEmpty() throws EmployeeException {
        EmployeeRepositoryInterface repo = new EmployeeMock();
        EmployeeValidator valid = new EmployeeValidator();
        EmployeeController ctrl = new EmployeeController(repo, valid);

        assertEquals(0, ctrl.getEmployeesList().size());

        Employee emp2 = new Employee("Un", "Nume", "123", DidacticFunction.TEACHER, "345");
        Employee emp2New = new Employee("Un", "Nume", "123", DidacticFunction.LECTURER, "345");

        boolean result;
        result = repo.modifyEmployee(emp2, emp2New);
        assertFalse(result);
    }
}